<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\MaestroCliente;
use App\Models\Cliente;
use App\Models\Ventum;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Moneda;

class MaestroClienteController extends Controller
{
    public $contador;

    public function __construct()
    {
        $this->middleware('auth');
         
    }
    

    public function index(Request $request){  
        if ($request->session()->has('alerta') and $request->session()->get('contador') == 1)
            {
                $alerta = $request->session()->get('alerta');
                $request->session()->put('contador', 0);
                
            }
        else{
            $alerta = "no traia valor";
        }
        
        Auth::user()->autorizarRol([1]);

        $maestro = MaestroCliente::select('maestro_cliente.*', 'cliente.*')
        ->join('cliente', 'maestro_cliente.id_cliente', '=', 'cliente.id_cliente')
        ->get();

        $pais = Pais::select("*")->orderBy("nombre_pais")->get();
        

        $moneda = Moneda::select('codigo', 'nombre_moneda', 'simbolo')->orderBy('nombre_moneda')->get();
        $estado = Estado::all();
        $municipio = Municipio::all();
        return view('maestros.maestroCliente', compact('maestro', 'pais', 'moneda', 'estado', 'municipio', 'alerta'));
    }

    public function getEstados(Request $request){
        Auth::user()->autorizarRol([1,2]);

        if($request->ajax()){
            $estados = Estado::where('id_pais', '=', $request->paisId)->get();
            foreach($estados as $estados){
                $estadoArray[$estados->id] = $estados->nombre_estado;
            }

            return response()->json($estadoArray);

        }

    }

    public function getMunicipios(Request $request){
        Auth::user()->autorizarRol([1,2]);

        if($request->ajax()){
            $municipios = Municipio::where('id_estado', '=', $request->estadoId)->get();
            foreach($municipios as $municipios){
                $municipioArray[$municipios->id] = $municipios->nombre_municipio;
            }

            return response()->json($municipioArray);

        }
    }

    public function getParaiso(Request $request)
    {
        Auth::user()->autorizarRol([1,2]);
        if($request->ajax()){

            $paraiso = Pais::where('id', '=', $request->paisId)->get();
            
            foreach($paraiso as $paraiso){
                $paraisoArray[$paraiso->id] = $paraiso->paraiso_fiscal;
            }

            return response()->json($paraisoArray);

        }

    }

    public function getParaisoEstado(Request $request){
        Auth::user()->autorizarRol([1,2]);
        if($request->ajax()){

            $paraiso = Estado::where('id', '=', $request->estadoId)->get();
            
            foreach($paraiso as $paraiso){
                $paraisoArray[$paraiso->id] = $paraiso->paraiso_fiscal;
            }

            return response()->json($paraisoArray);

        }
    }
    public function update_form($id){

        $cliente = MaestroCliente::select('maestro_cliente.*', 'cliente.*')
        ->where('id_maestro_cliente', $id)
        ->join('cliente', 'maestro_cliente.id_cliente', '=', 'cliente.id_cliente')
        ->get();
        
        return view('maestros.update.cliente', compact('cliente'));

        // return response()->json($maestro);
    }
    public function cliente($id){

        $maestro = MaestroCliente::select('maestro_cliente.*', 'cliente.*')
        ->where('id_maestro_cliente', $id)
        ->join('cliente', 'maestro_cliente.id_cliente', '=', 'cliente.id_cliente')
        ->get();
        
        return view('maestros.detalles.cliente', compact('maestro'));

        // return response()->json($maestro);
    }

    public function store(Request $request){

        

        Auth::user()->autorizarRol([1]);

        if($request->pais == 55){
            $d = 16;
        } else{
            $d = 15;
        }

        $validacion = $request->validate([
            'condiciones_credito' => 'max:400',
            'nombre_cliente' => 'max:200',
            'numero_cliente_icg' => 'max:100',
            'numero_cliente' => 'max:100',
            'nombre_comercial' => 'required|max:100',
            'nombre_del_sujeto' => 'max:100',
            'direccion' => 'max:400',
            'pais' =>'max:100',
            'codigo_pais' =>'max:100',
            'ciudad' =>'max:100',
            'departamento' =>'max:100',
            'municipio' =>'max:100',
            'telefono_fijo' =>'max:'.$d,
            'pagina_web' =>'max:200',
            'correo' =>'nullable|email|max:100',
            'telefono_celular' =>'|max:'.$d,
            'paraiso_fiscal' =>'max:100',
            'nombre_contacto' =>'max:100',
            'telefono_contacto' =>'|max:'.$d,
            'cargo_contacto' =>'max:100',
            'pagina_web_contacto' => 'max:100',
            'correo_contacto' =>'nullable|email|max:100',
            'moneda_principal' =>'max:100',
            'tipo_cambio' =>'max:100',
            'giro_fical_negocio' =>'max:100',
            'tipo_contribuyente' =>'max:100',
            'nit_niff' =>'max:100',
            'n_registro_fiscal' =>'max:100',
            'cobra_iva' =>'max:100',
            'entera_iva' =>'max:100',
            'porc_retencion' => 'numeric|min:0.00',
            'percepcion' =>'max:100',
            'cta_pasivo_uno' =>'max:100',
            'cta_pasivo_dos' =>'max:100',
            'cta_activo_uno' =>'max:100',
             'cta_activo_dos' => 'max:100',
            'comision' => 'numeric|min:0.00',
            'emitira_nc' =>'max:2',
            'condiciones_operacion' =>'max:200',
        ]);

        $cliente = new Cliente();
        $cliente->nombre_cliente = $request->nombre_cliente;
        $cliente->save();

        MaestroCliente::create(array_merge($validacion, ['id_cliente' => $cliente->id_cliente]));


        $request->session()->put('alerta', 'create');
        $request->session()->put('contador', 1);
            
        //return $this->alert;
        return redirect()->route('maestroCliente.index');
    }
    






        public function update(Request $request){
            Auth::user()->autorizarRol([1]);

            

            if($request->pais == "Estados Unidos"){
                $d = 16;
            } else{
                $d = 15;
            }

            // $request->session()->put('alerta', 'modal');
            // $request->session()->put('contador', 1);
    

            $validacion = $request->validate([
                'condiciones_credito' => 'max:400',//-----------
                'id_maestro_cliente' => 'required',
                'id_cliente' => 'required',
                'nombre_cliente' => 'max:200',
                'numero_cliente_icg' => 'max:100',
                'numero_cliente' => 'max:200', //----
                'nombre_comercial' => 'max:300',

                'nombre_del_sujeto' => 'max:200',
                'direccion' => 'max:400', //---------------
                'pais' =>'max:100',
                'codigo_pais' =>'max:100',
                'ciudad' =>'max:100',
                'departamento' =>'max:100',
                'municipio' =>'max:100',
                'telefono_fijo' =>'max:'.$d, 
                'pagina_web' =>'max:200', //----------------------
                'correo' =>'nullable|email|max:100',
                'telefono_celular' =>'max:'.$d,// ------------------------
                'paraiso_fiscal' =>'max:100',
                'nombre_contacto' =>'max:100',
                'telefono_contacto' =>'max:'.$d,
                'cargo_contacto' =>'max:100',
                'pagina_web_contacto' => 'max:200', ///-----------------
                'correo_contacto' =>'nullable|email|max:100',
                'moneda_principal' =>'max:100',
                'tipo_cambio' =>'max:100',
                'giro_fical_negocio' =>'max:100',
                'tipo_contribuyente' =>'max:100',
                'nit_niff' =>'max:100',
                'n_registro_fiscal' =>'max:100',
                'cobra_iva' =>'max:2',
                'entera_iva' =>'max:2',
                'porc_retencion' => 'numeric|min:0.00',
                'percepcion' =>'max:100',
                'cta_pasivo_uno' =>'max:100',
                'cta_pasivo_dos' =>'max:100',//-----------
                'cta_activo_uno' =>'max:100',
                'cta_activo_dos' =>'max:100',//-----------
                'comision' => 'numeric|min:0.00',
                'emitira_nc' =>'max:100',
                'condiciones_operacion' =>'max:200',//-----------
            ]);

            $cliente = Cliente::find($request->id_cliente);
            $cliente->nombre_cliente = $request->nombre_cliente;
            $cliente->save();

            $maestro = Maestrocliente::find($request->id_maestro_cliente);

            $maestro->fill($request->all());
            
            $maestro->save();

            $request->session()->put('alerta', 'update');
            $request->session()->put('contador', 1);
        
            //return $this->alert;
            return redirect()->route('maestroCliente.index');
        }

        public function destroy(Request $request){
            Auth::user()->autorizarRol([1]);

            $validacion = $request->validate([
                'did_maestro_cliente' => 'required',
                'did_cliente' => 'required',
            ]);

            $ventasCliente = Ventum::where("id_cliente", '=', $request->did_cliente )->get();

            //Validamos que no tengan ventas ligaddas
            if($ventasCliente->count() == 0){

                $maestro = Maestrocliente::find($request->did_maestro_cliente);
                $maestro->delete();

                $cliente = Cliente::find($request->did_cliente);
                $cliente->delete();

                

                $request->session()->put('alerta', 'delete');
                $request->session()->put('contador', 1);

            }else
            {
                $request->session()->put('alerta', 'deleteError');
                $request->session()->put('contador', 1);
            }

            return redirect()->route('maestroCliente.index');
        }


}
