<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\MaestroCliente;
use App\Models\Cliente;
use App\Models\Ventum;
use App\Models\Compra;
use App\Models\CuentasCobrar;
use App\Models\CuentasPagar;
use App\Models\DetalleVentum;
use App\Models\Proveedor;


class VentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
         
    }

    public function ventas(){

        return view('gestion-ventas.index');
    }

    public function Addventa(){

        return view('gestion-ventas.venta');
    }

    public function index(Request $request){  
        Auth::user()->autorizarRol([1, 2]);

        //Validación para alertas de toastr
        if ($request->session()->has('alerta') and $request->session()->get('contador') == 1)
            {
                $alerta = $request->session()->get('alerta');
                $request->session()->put('contador', 0);
                
            }
        else{
            $alerta = "no traia valor";
        }

        //Ventas y clientes
        $ventas = Ventum::select('venta.*', 'cliente.*')
        ->join('cliente', 'venta.id_cliente', '=', 'cliente.id_cliente')
        ->get();

        //Cliente para select
        $clientes = Cliente::select('*')->orderBy('nombre_cliente')->get();

        return view('venta.venta', compact('ventas', 'clientes', 'alerta'));
    }

    public function venta($id){  
        Auth::user()->autorizarRol([1, 2]);

        //Ventas y clientes
        $venta = Ventum::find($id);

        //Cliente para select
        $clientes = Cliente::select('*')->orderBy('nombre_cliente')->get();

        return view('venta.update.venta', compact('venta', 'clientes'));
    }

    public function store(Request $request){
        Auth::user()->autorizarRol([1, 2]);

        //Validaciones de Laravel
        $validacion = $request->validate([
            'id_cliente' => 'required',
            'credito_fiscal' => 'unique:venta',
            'monto_ven' => 'min:0.01',
        ]);

        //Cuenta X cobrar de la venta
        if ($request->estado_venta = 'Quedan' | $request->estado_venta = ''){

            $cuentaCobrar = new CuentasCobrar();

            $cuentaCobrar->monto_cobrar = $request->monto_ven;
            $cuentaCobrar->fecha_vencimiento_monto = $request->fecha_vencimiento;
            $cuentaCobrar->save();
        }
        
        //store ventas
        $venta = new Ventum();
        
        //CAMBIAR ID DE CUENTAS POR COBRAR!!!!!!!
        $venta->estado_venta = 'Pendiente';
        $venta->id_cuenta_cobrar = $cuentaCobrar->id_cuenta_cobrar;
        $venta->fill($request->all());

        $venta->save();

        //guardamos las seciones para las alertas de toastr	
        $request->session()->put('alerta', 'create');
        $request->session()->put('contador', 1);

        return redirect()->route('venta.index');
    }

    public function selectSearch(Request $request)
    {
        $clientes = [];

        if($request->has('q')){
            $search = $request->q;
            $clientes =Cliente::select("id_cliente", "nombre_cliente")
                    ->where('nombre_cliente', 'LIKE', "%$search%")
                    ->get();
        }
        return response()->json($clientes);
    }

    public function venta_compras($id){

        $compras = Compra::where('id_venta', $id)->get();

        $proveedores = Proveedor::all();
        $ventas = Ventum::all();
        $clientes = Cliente::all();
        $cxp = CuentasPagar::all();
        $cxc = CuentasCobrar::all();
        $alerta = '';

        return view('compra.compra', compact('compras', 'proveedores', 'ventas', 'clientes', 'cxc', 'cxp', 'alerta'));
    }


    //function to update the ventas

    public function update(Request $request){
        Auth::user()->autorizarRol([1]);

        //Validaciones de Laravel
        $validacion = $request->validate([
            'id_venta' => 'required',
            'id_cliente' => 'required',
            'credito_fiscal' => 'unique:App\Models\Ventum,credito_fiscal,'.$request->id_venta,
            'monto_ven' => 'min:0.01'
        ]);

        //update ventas
        $venta = Ventum::find($request->id_venta);

        $venta->fill($request->all());
        $venta->save();

        //update CXC
        if($request->id_cuenta_cobrar){
            
            $cuenta = CuentasCobrar::find($venta->id_cuenta_cobrar);
            $cuenta->monto_cobrar = $request->monto_ven; 
            $cuenta->fecha_vencimiento_monto = $request->fecha_vencimiento; 
            $cuenta->save();
        }

        
        $request->session()->put('alerta', 'update');
        $request->session()->put('contador', 1);

        return redirect()->route('venta.index');
    }







    //function to delete the ventas

    public function destroy(Request $request){
        Auth::user()->autorizarRol([1]);

        //Validaciones de Laravel
        $validacion = $request->validate([
            'dId_venta' => 'required',
        ]);

        //Eliminar venta
        $venta = Ventum::find($request->dId_venta);
        
        //valida si tiene filas en alguna otra tabla para que no se pueda eliminar
        $compra = Compra::Select('*')->where('id_venta', '=', $request->dId_venta);
        $detalle = DetalleVentum::Select('*')->where('id_venta', '=', $request->dId_venta);
        

        
        if(!($compra->count() > 0 || $detalle->count() > 0)){
            //eliminamos la cuenta por cobrar
            $cuentaCobrar = CuentasCobrar::find($venta->id_cuenta_cobrar);
            $cuentaCobrar->delete();
            $venta->delete();
            
            $request->session()->put('alerta', 'delete');
            $request->session()->put('contador', 1);
        }else{
            $request->session()->put('alerta', 'errorDelete');
            $request->session()->put('contador', 1);
        }

        return redirect()->route('venta.index');
    }

    






    //This is a function to pay the ventas
    public function pay(Request $request){
        Auth::user()->autorizarRol([1]);

        //Validaciones de Laravel
        $validacion = $request->validate([
            'pId_venta' => 'required',
            'fecha_pago'=>'required',
        ]);

        //pay ventas
        $venta = Ventum::find($request->pId_venta);
        $venta->fecha_pago = $request->fecha_pago;
        $venta->estado_venta = 'Pagada';
        $venta->save();

        //pay CxC
        $cuenta = CuentasCobrar::find($venta->id_cuenta_cobrar);
        $cuenta->fecha_pago_monto = $request->fecha_pago;
        $cuenta->save();
        
        $request->session()->put('alerta', 'pay');
        $request->session()->put('contador', 1);

        return redirect()->route('venta.index');
    }

    public function editPay(Request $request){
        Auth::user()->autorizarRol([1]);

        //Validaciones de Laravel
        $validacion = $request->validate([
            'epId_venta' => 'required',
            'epFecha_pago'=>'required',
        ]);

        //edit pay venta
        $venta = Ventum::find($request->epId_venta);
        $venta->fecha_pago_venta = $request->epFecha_pago;
        $venta->save();

        //edit pay CxC
        $cuenta = CuentasCobrar::find($venta->id_cuenta_cobrar);
        $cuenta->fecha_pago_monto = $request->epFecha_pago;
        $cuenta->save();
        
        $request->session()->put('alerta', 'editPay');
        $request->session()->put('contador', 1);

        return redirect()->route('venta.index');
    }

}

//editPay
