<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\NT;

class AdministradorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        Auth::user()->autorizarRol(1);

        $notas = Nt::all();

        return view('notascredito.index', compact('notas'));
    }
}
