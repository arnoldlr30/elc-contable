<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\MaestroProveedor;
use App\Models\Proveedor;
use App\Models\Compra;
use App\Models\Pais;
use App\Models\Estado;
use App\Models\Moneda;
use App\Models\Municipio;


class MaestroProveedorController extends Controller
{
    public $contador;

    public function __construct()
    {
        $this->middleware('auth');
         
    }
    

    public function index(Request $request){

        Auth::user()->autorizarRol([1]);
        
        if ($request->session()->has('alerta') and $request->session()->get('contador') == 1)
            {
                $alerta = $request->session()->get('alerta');
                $request->session()->put('contador', 0);
                
            }
        else{
            $alerta = "no traia valor";
        }
    
        $pais = Pais::select("*")->orderBy("nombre_pais")->get();

        $maestro = MaestroProveedor::select('maestro_proveedor.*', 'proveedor.*')
        ->join('proveedor', 'maestro_proveedor.id_proveedor', '=', 'proveedor.id_proveedor')
        ->get();
        $moneda = Moneda::select('codigo', 'nombre_moneda', 'simbolo')->orderBy('nombre_moneda')->get();
        $estado = Estado::all();
        $municipio = Municipio::all();

        return view('maestros.maestroProveedor', compact('maestro', 'alerta', 'pais', 'estado', 'municipio', 'moneda'));
    }

    public function update_form($id){

        $maestro = MaestroProveedor::select('maestro_proveedor.*', 'proveedor.*')
        ->where('id_maestro_proveedor', $id)
        ->join('proveedor', 'maestro_proveedor.id_proveedor', '=', 'proveedor.id_proveedor')
        ->get();
        $pais = Pais::all();
        $moneda = Moneda::select('codigo', 'nombre_moneda', 'simbolo')->orderBy('nombre_moneda')->get();
        $estado = Estado::all();
        $municipio = Municipio::all();

        return view('maestros.update.proveedor', compact('maestro', 'pais', 'moneda', 'estado', 'municipio'));

        // return response()->json($maestro);
    }
    public function proveedor($id){

        $maestro = MaestroProveedor::select('maestro_proveedor.*', 'proveedor.*')
        ->where('id_maestro_proveedor', $id)
        ->join('proveedor', 'maestro_proveedor.id_proveedor', '=', 'proveedor.id_proveedor')
        ->get();
        $pais = Pais::all();
        $moneda = Moneda::select('codigo', 'nombre_moneda', 'simbolo')->orderBy('nombre_moneda')->get();
        $estado = Estado::all();
        $municipio = Municipio::all();

        return view('maestros.detalles.proveedor', compact('maestro', 'pais', 'moneda', 'estado', 'municipio'));

        // return response()->json($maestro);
    }



    public function store(Request $request){
        Auth::user()->autorizarRol([1]);

        if($request->pais == 55){
            $d = 16;
        } else{
            $d = 15;
        }

        $validacion = $request->validate([
            
            'nombre_proveedor' => 'required|max:100',
            'numero_proveedor_icg' => 'max:50',
            'numero_proveedor' => 'max:50',
            'nombre_comercial' => 'max:100',
            
            'direccion' => 'max:400',
            'pais' =>'max:50',
            'codigo_pais' =>'max:50',
            'ciudad' =>'max:50',
            'departamento' =>'max:50',
            'municipio' =>'max:50',
            'municipio' =>'max:50',
            'telefono_fijo' =>'max:'.$d,
            'pagina_web' =>'max:200',
            'correo' =>'nullable|email|max:50',
            'telefono_celular' =>'|max:'.$d,
            'paraiso_fiscal' =>'max:50',
            'nombre_contacto' =>'max:50',
            'telefono_contacto' =>'|max:'.$d,
            'cargo_contacto' =>'max:50',
            'pagina_web_contacto' => 'max:50',
            'correo_contacto' =>'nullable|email|max:50',
            'moneda_principal' =>'max:50',
            'tipo_cambio' =>'max:50',
            'giro_fical_negocio' =>'max:50',
            'tipo_contribuyente' =>'max:50',
            'nit_niff' =>'max:50',
            'n_registro_fiscal' =>'max:50',
            'cobra_iva' =>'max:50',
            'entera_iva' =>'max:50',
            'porc_retencion' => 'numeric|min:0.00',
            'percepcion' =>'max:50',
            'cta_pasivo_uno' =>'max:50',
            'cta_pasivo_dos' =>'max:50',
            'cta_activo_uno' =>'max:50',
             'cta_activo_dos' => 'max:50',
            'comision' => 'numeric|min:0.00',
            'emitira_nc' =>'max:2',
            'condiciones_operacion' =>'max:200',
            'municipio' =>'max:50',       
        ]);

        $proveedor = new Proveedor();

        $proveedor->nombre_proveedor = $request->nombre_proveedor;
        $proveedor->save();

        MaestroProveedor::create(array_merge($validacion, ['id_proveedor' => $proveedor->id_proveedor]));

        $request->session()->put('alerta', 'create');
        $request->session()->put('contador', 1);
            
        //return $this->alert;
        return redirect()->route('maestroProveedor.index');
        }






        public function update(Request $request){
            Auth::user()->autorizarRol([1]);


            if($request->pais == "Estados Unidos"){
                $d = 16;
            } else{
                $d = 15;
            }

            $validacion = $request->validate([
                'condiciones_credito' => 'max:200',//-----------
                'nombre_del_sujeto' => 'max:50',
                'direccion' => 'max:200', //---------------
                'pais' =>'max:50',
                'codigo_pais' =>'max:50',
                'ciudad' =>'max:50',
                'departamento' =>'max:50',
                'municipio' =>'max:50',
                'telefono_fijo' =>'max:'.$d, 
                'pagina_web' =>'max:200', //----------------------
                'correo' =>'nullable|email|max:50',
                'telefono_celular' =>'max:'.$d,// ------------------------
                'paraiso_fiscal' =>'max:50',
                'nombre_contacto' =>'max:50',
                'telefono_contacto' =>'max:'.$d,
                'cargo_contacto' =>'max:50',
                'pagina_web_contacto' => 'max:200', ///-----------------
                'correo_contacto' =>'nullable|email|max:50',
                'moneda_principal' =>'max:50',
                'tipo_cambio' =>'max:50',
                'giro_fical_negocio' =>'max:50',
                'tipo_contribuyente' =>'max:50',
                'nit_niff' =>'max:50',
                'n_registro_fiscal' =>'max:50',
                'cobra_iva' =>'max:2',
                'entera_iva' =>'max:2',
                'porc_retencion' => 'required|numeric|min:0.00',
                'percepcion' =>'max:50',
                'cta_pasivo_uno' =>'max:50',
                'cta_pasivo_dos' =>'max:50',//-----------
                'cta_activo_uno' =>'max:50',
                'cta_activo_dos' =>'max:50',//-----------
                'comision' => 'required|numeric|min:0.00',
                'emitira_nc' =>'max:50',
                'condiciones_operacion' =>'max:200',//-----------
            ]);

            $proveedor = Proveedor::find($request->id_proveedor);
            $proveedor->nombre_proveedor = $request->nombre_proveedor;
            $proveedor->save();

            $maestro = Maestroproveedor::find($request->id_maestro_proveedor);
            $maestro->fill($request->all());
            $maestro->save();

            $request->session()->put('alerta', 'update');
            $request->session()->put('contador', 1);
        
            //return $this->alert;
            return redirect()->route('maestroProveedor.index');
        }

        public function destroy(Request $request){
            Auth::user()->autorizarRol([1]);

            $validacion = $request->validate([
                'did_maestro_proveedor' => 'required',
                'did_proveedor' => 'required',
            ]);

            $ventasProveedor = Compra::where("id_proveedor", '=', $request->did_proveedor )->get();

            if($ventasProveedor->count() == 0){

                $maestro = Maestroproveedor::find($request->did_maestro_proveedor);
                $maestro->delete();

                $proveedor = Proveedor::find($request->did_proveedor);
                $proveedor->delete();

                

                $request->session()->put('alerta', 'delete');
                $request->session()->put('contador', 1);

            }else
            {
                $request->session()->put('alerta', 'deleteError');
                $request->session()->put('contador', 1);
            }

            return redirect()->route('maestroProveedor.index');
        }


}
