<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Cliente;
use App\Models\Ventum;
use App\Models\NT;

class NotasCreditoController extends Controller
{
    public function index (){  
        Auth::user()->autorizarRol([1]);

        $notas = Nt::all();

        return view('notascredito.index', compact('notas'));
    }

    public function nota(){  

    	$clientes = Cliente::select('*')->orderBy('nombre_cliente')->get();

    	$ventas = Ventum::select('*')->orderBy('id_venta')->get();

        return view('notascredito.nota', compact('clientes', 'ventas'));
    }

    public function store(Request $request){

    	Auth::user()->autorizarRol([1, 2]);

        //Validaciones de Laravel
        $validacion = $request->validate([
            'ncr' => 'required',
            'monto' => 'required',
            'concepto' => 'required',
            'fecha_emision' => 'required',
            'id_factura' => 'required',
            'id_cliente' => 'required',
        ]);

        //store ventas
        $NT = new NT();

       	$NT->fill($request->all());
        $NT->save();

        //guardamos las seciones para las alertas de toastr	
        $request->session()->put('alerta', 'create');

        return redirect()->route('notas-credito.index');
    }

    public function edit_form($id){

        $nota = NT::find($id);

        $clientes = Cliente::select('*')->orderBy('nombre_cliente')->get();

        $ventas = Ventum::select('*')->orderBy('id_venta')->get();

        return view('notascredito.update', compact('nota', 'clientes', 'ventas'));
    }

    public function edit(Request $request, $id){
        
        $validacion = $request->validate([
            'ncr' => 'required',
            'monto' => 'required',
            'concepto' => 'required',
            'fecha_emision' => 'required',
            'id_factura' => 'required',
            'id_cliente' => 'required',
        ]);

        //update notas
        $nota = NT::find($id);

        $nota->fill($request->all());
        $nota->save();

        $request->session()->put('alerta', 'update');
        $request->session()->put('contador', 1);

        return redirect()->route('notas-credito.index');
    }

    public function delete(Request $request){
        Auth::user()->autorizarRol([1]);

        $nota = NT::find($request->dId_nt);

        $nota->delete();

        return redirect()->route('notas-credito.index');
        
        $request->session()->put('alerta', 'delete');
        $request->session()->put('contador', 1);
    }

    public function destroy(Request $request){

        $nota = NT::find($request->id_nt);

        $nota->delete();

        $request->session()->put('alerta', 'delete');
        $request->session()->put('contador', 1);
    }
}
