<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class nota credito
 * 
 * @property int $id_nt
 * @property int $ncr
 * @property int $monto
 * @property Carbon $fecha_emision
 * @property int $id_factura
 * @property int $id_cliente
 * @property string $anulada
 * @property string $concepto
 * 
 * 
 *
 * @package App\Models
 */

class NT extends Model
{
    protected $table = 'notas_credito';
	protected $primaryKey = 'id_nt';
	public $timestamps = false;

	protected $dates = [
		'fecha_emision',
	];

	protected $fillable = [
		'id_cliente',
		'monto',
		'fecha_emision',
		'ncr',
		'id_factura',
		'id_cliente',
		'anulada',
		'concepto',
	];

	protected $appends = ['factura', 'cliente'];

	public function venta()
	{
		return $this->belongsTo(Ventum::class, 'id_factura');
	}

	public function cliente(){

		return $this->belongsTo(Cliente::class, 'id_cliente');
	}

	public function getFacturaAttribute(){

		return $this->venta()->pluck('credito_fiscal')->first();
	}
	public function getClienteAttribute(){

		return $this->cliente()->pluck('nombre_cliente')->first();
	}
}
