<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ventum
 * 
 * @property int $id_venta
 * @property int $id_cliente
 * @property int $id_cuenta_cobrar
 * @property string $credito_fiscal
 * @property float|null $monto_ven
 * 
 * @property string $concepto_ven
 * @property Carbon $fecha_emision
 * @property Carbon $fecha_vencimiento
 * @property Carbon $fecha_quedan
 * @property string $orden_compra
 * @property string $presupuesto
 * @property string $mercaderia
 * @property string $estado_venta
 * @property Carbon $fecha_pago
 * @property int $num_cheque
 * @property int $num_mandamiento
 * 
 *
 * 
 * @property Cliente $cliente
 * @property Collection|Compra[] $compras
 *
 * @package App\Models
 */
class Ventum extends Model
{
	protected $table = 'venta';
	protected $primaryKey = 'id_venta';
	public $timestamps = false;

	protected $casts = [
		'id_cliente' => 'int',
		'id_cuenta_cobrar' => 'int',
		'monto_ven' => 'float'
	];

	protected $dates = [
		'fecha_emision',
		'fecha_vencimiento',
		'fecha_quedan',
		'fecha_pago',
	];

	protected $fillable = [
		'id_cliente',
		'id_cuenta_cobrar',
		'credito_fiscal',
		'monto_ven',
		'fecha_pago_venta',
		'concepto_ven',
		'fecha_emision',
		'fecha_vencimiento',
		'anulada',
		'orden_compra',
		'presupuesto',
		'mercaderia',
		'estado_venta',
		'fecha_quedan',
		'fecha_pago',
		'num_mandamiento',
		'num_cheque',
	];

	protected $appends = ['cliente'];

	public function getClienteAttribute(){

		return $this->cliente()->pluck('nombre_cliente')->first();
	}

	public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'id_cliente');
	}

	public function nota_credito()
	{
		return $this->belongsTo(NT::class, 'id_venta');
	}

	public function compras()
	{
		return $this->hasMany(Compra::class, 'id_venta');
	}
}
