<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NotasCreditoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas_credito', function (Blueprint $table) {
            $table->increments('id_nt');
            $table->integer('nota_credito');
            $table->integer('monto');
            $table->date('fecha_emision');
            $table->integer('id_factura');
            $table->integer('id_cliente')->nullable();
            $table->boolean('anulada');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('usuarios');
    }
}
