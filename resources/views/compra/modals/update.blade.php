<!-- Modal -->
<div class="modal fade " id="compraModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Actualizar datos de la compra</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('compra.update')}}" autocomplete="of" method="POST">
                
                @csrf
                @method('PUT')
               
                <div class="container">
                    <input type="hidden" name="uid_compra" id="uid_compra">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Proveedor</label>
                            <select class="select-css" name="uid_proveedor" id="uid_proveedor">
                                <option value="">Seleccione un proveedor</option>
                                @foreach ($proveedores as $p)
                                    <option value="{{$p->id_proveedor}}" {{ old('uid_proveedor') == $p->id_proveedor ? 'selected' : '' }}>{{$p->nombre_proveedor}}</option>
                                @endforeach
                            </select>
                            @error('uid_proveedor')
                            <small>*{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label>CF de la venta relacionada</label>
                            <select class="select-css" name="uid_venta" id="uid_venta">
                                <option value="">Seleccione una venta para la compra</option>
                                @foreach ($ventas as $v)
                                    <option value="{{$v->id_venta}}" {{ old('uid_venta') == $v->id_venta ? 'selected' : '' }}>{{$v->credito_fiscal}}</option>
                                @endforeach
                            </select>
                            @error('uid_venta')
                            <small>*{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-6 mb-3">
                <label>CxP</label>
                <select id="uid_cuenta_pagar" class="select-css" name="uid_cuenta_pagar" >
                    <option value="">Seleccione una CxP</option>
                    @foreach ($cxp as $c)
                        <option value="{{$c->id_cuenta_pagar}}" {{ old('uid_cuenta_pagar') == $c->id_cuenta_pagar ? 'selected' : '' }}>{{$c->retencion}}</option>
                    @endforeach
                </select>
                @error('uid_cuenta_pagar')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-6 mb-3">
                <label>CxC</label>
                <select id="uid_cuenta_cobrar" class="select-css" name="uid_cuenta_cobrar" >
                    <option value="">Seleccione una CxC</option>
                    @foreach ($cxc as $c)
                        <option value="{{$c->id_cuenta_cobrar}}" {{ old('uid_cuenta_cobrar') == $c->id_cuenta_cobrar ? 'selected' : '' }}>{{$c->monto_cobrar}}</option>
                    @endforeach
                </select>
                @error('uid_cuenta_cobrar')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
                        <div class="col-md-6">
                            <label>Crédito Fiscal</label>
                            <input type="text" class="txt-form" name="ucredito_fiscal" id="ucredito_fiscal" value="{{old('ucredito_fiscal')}}">
                            @error('ucredito_fiscal')
                                <small>*{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label>Monto de la compra</label>
                            <input type="text" class="txt-form" name="umonto_com" id="umonto_com"  value="{{old('umonto_com')}}">
                            @error('umonto_com')
                                <small>*{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label>Concepto</label>
                            <input type="text" class="txt-form" name="uconcepto_com" id="uconcepto_com" value="{{old('uconcepto_com')}}">
                            @error('uconcepto_com')
                            <small>*{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label>Fecha emisión</label>
                            <input type="date" class="txt-form" name="ufecha_emision" id="ufecha_emision"  value="{{old('ufecha_emision')}}">
                            @error('ufecha_emision')
                            <small>*{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label>Fecha vencimiento</label>
                            <input type="date" class="txt-form" name="ufecha_vencimiento" id="ufecha_vencimiento" value="{{old('ufecha_vencimiento')}}">
                            @error('ufecha_vencimiento')
                            <small>*{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label>Sub-total</label>
                            <input type="number" step="any" value="{{old('usub_total')}}" class="txt-form" name="usub_total" id="usub_total">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Actualizar</button>
                </div>
            </form>

        </div>
    </div>

</div>