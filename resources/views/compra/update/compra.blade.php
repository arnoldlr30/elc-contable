@extends('layouts.main', ['activePage' => 'compra', 'titlePage' => __('Editar Compra')])
@section('title', 'Editar compras')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }    
</style>


@include('compra.update.update')

@endsection

@push('js')

<script>
   
    
</script>

<script src="{{asset('assets/js/maestroCliente.js')}}"></script>
<script src="{{asset('assets/js/validaciones/vCliente.js')}}"></script>

@endpush