@extends('layouts.main', ['activePage' => 'compras-list', 'titlePage' => __('Nueva Compra')])
@section('title', 'Compra')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }    
</style>


@include('compra.form')

@endsection

@push('js')

<script>
   
    
</script>

<script src="{{asset('assets/js/maestroCliente.js')}}"></script>
<script src="{{asset('assets/js/validaciones/vCliente.js')}}"></script>

@endpush