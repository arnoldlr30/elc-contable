<div>
    <form action="{{route('compra.create')}}" method="POST">
        @csrf
        <!--Hidden inputs-->
        <input type="hidden" name="id_compra">
        <div class="row">
            <div class="col-6 mb-3">
                <label>Proveedor</label>
                <select class="livesearch form-control p-3" name="id_proveedor" id="livesearch">
                    
                </select>
                {{-- <select id="" class="select-css" name="id_proveedor" >
                    <option value="">Seleccione un proveedor</option>
                    @foreach ($proveedores as $proveedor)
                        <option value="{{$proveedor->id_proveedor}}" {{ old('id_proveedor') == $proveedor->id_proveedor ? 'selected' : '' }}>{{$proveedor->nombre_proveedor}}</option>
                    @endforeach
                </select> --}}
                @error('id_proveedor')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-6 mb-3">
                <label>CF de Venta relacionada</label>
                <select class="livesearch2 form-control p-3" name="id_venta" id="livesearch2">
                    
                </select>
                {{-- <select id="" class="select-css" name="id_venta" >
                    <option value="">Seleccione un venta</option>
                    @foreach ($ventas as $venta)
                        <option value="{{$venta->id_venta}}" {{ old('id_venta') == $venta->id_venta ? 'selected' : '' }}>{{$venta->credito_fiscal}} - {{$venta->concepto_ven}}</option>
                    @endforeach
                </select> --}}
                @error('id_venta')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-6 mb-3">
                <label>CxP</label>
                <select class="livesearch3 form-control p-3" name="id_cuenta_pagar" id="livesearch3">
                    
                </select>
                {{-- <select id="" class="select-css" name="id_cuenta_pagar" >
                    <option value="">Seleccione una CxP</option>
                    @foreach ($cxp as $c)
                        <option value="{{$c->id_cuenta_pagar}}" {{ old('id_cuenta_pagar') == $c->id_cuenta_pagar ? 'selected' : '' }}>{{$c->retencion}}</option>
                    @endforeach
                </select> --}}
                @error('id_cuenta_pagar')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-6 mb-3">
                <label>CxC</label>
                <select class="livesearch4 form-control p-3" name="id_cuenta_cobrar" id="livesearch4">
                    
                </select>
                {{-- <select id="" class="select-css" name="id_cuenta_cobrar" >
                    <option value="">Seleccione una CxC</option>
                    @foreach ($cxc as $c)
                        <option value="{{$c->id_cuenta_cobrar}}" {{ old('id_cuenta_cobrar') == $c->id_cuenta_cobrar ? 'selected' : '' }}>{{$c->monto_cobrar}}</option>
                    @endforeach
                </select> --}}
                @error('id_cuenta_cobrar')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Crédito fiscal</label>
                <input type="text" class="txt-form" name="credito_fiscal">
                @error('descripcion')
                    <small>*{{$message}}</small>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Monto</label>
                <input type="number" class="txt-form" name="monto_com">
                @error('')
                    <small>*{{$message}}</small>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Concepto</label>
                <input type="text" class="txt-form" name="concepto_com">
                @error('cantidad')
                    <small>*{{$message}}</small>
                @enderror
            </div>
        <div class="col-6">
                <label>Fecha Emisión</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_emision" value="{{old('fecha_emision')}}">
                @error('fecha_emision')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-6">
                <label>Fecha Vencimiento</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_vencimiento" value="{{old('fecha_vencimiento')}}">
                @error('fecha_vencimiento')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-md-6">
                <label>Sub-total</label>
                <input type="number" step="any" class="txt-form" name="sub_total" id="subTotal">
            </div>
        </div>
        <div class="mt-5 row justify-content-center">
            <input type="submit" value="Agregar compra" class="btn btn-radius btn-info" >
        </div>
    </form>
</div>

<script type="text/javascript">
    $('.livesearch').select2({
        placeholder: 'Seleccione un proveedor',
        
        ajax: {
            url: '/autocomplete-search-pro',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nombre_proveedor,
                            id: item.id_proveedor
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>

<script type="text/javascript">
    $('.livesearch2').select2({
        placeholder: 'Seleccione una venta',
        
        ajax: {
            url: '/ajax-autocomplete-search-venta',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.credito_fiscal,
                            id: item.id_venta
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
<script type="text/javascript">
    $('.livesearch3').select2({
        placeholder: 'Seleccione una CxP',
        
        ajax: {
            url: '/ajax-autocomplete-search-cxp',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.retencion,
                            id: item.id_cuenta_pagar
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('.livesearch4').select2({
        placeholder: 'Seleccione una CxC',
        
        ajax: {
            url: '/ajax-autocomplete-search-cxc',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.monto_cobrar,
                            id: item.id_cuenta_cobrar
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>