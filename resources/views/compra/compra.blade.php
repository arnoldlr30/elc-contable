+@extends('layouts.main', ['activePage' => 'compra', 'titlePage' => __('Compras')])
@section('title', 'Compras de la venta')
@section('content')

<style>
    small{
        color: red;
        font-size: 15px;
    }

    button{
        background: none;
        color: inherit;
        border: none;
        padding: 0;
        font: inherit;
        cursor: pointer;
        outline: inherit;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<div class="mb-4">
    <x-table>
        <div class="row justify-content-center pt-5">
            <h3 class="">Compras de la venta:  <b>{{-- {{$venta->credito_fiscal}} --}}</b></h3>
        </div>

        <a href="{{ Route('new-compra') }}" type="button" class="btn btn-info">Nueva compra</a>

        {{-- <div id="divNuevaCompra">
            @include('compra.nuevaCompra')
        </div>  --}}
        
    <div class="m-5">
        @include('compra.compraTable')
        @include('compra.modals.update')
        @include('compra.modals.delete')
        @include('compra.modals.iva')
        @include('compra.modals.retencion')
        @include('compra.modals.comision')
    </div>
       
    </x-table>
</div>

@endsection

@push('js')
    
    @if ($errors->any())
        <script>
            toastr["error"]("Hay errores en los datos ingresados, Por favor echa un vistaso", "Error");
        </script>
        
    @endif

    @if ($alerta == "create")
        <script>
            toastr["success"]("Compra Creada correctamente", "Operación correcta");
        </script>
    @endif
    @if ($alerta == "delete")
        <script>
            toastr["warning"]("Compra eliminada correctamente", "Operación correcta");
        </script>
    @endif
    
    @if ($alerta == "errorDelete")
        <script>
            toastr["error"]("Tienes que eliminar antes los detalles de esta compra", "No se pudo eliminar");
        </script>
    @endif

    @if ($alerta == "update")
        <script>
            toastr["success"]("Compra actualizada correctamente", "Operación correcta");
        </script>
    @endif

<script>

        $(document).ready(function(){
            
            $('#compraTable').DataTable({
                
                responsive: true,
                autowidth: false,
        
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "Nada encontrado - disculpa :(",
                    "info": "Mostrando la página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "next": "Siguiente",
                        "previous": "Anterior",
                    }
                }
                   
            });

            let NewCompra = document.getElementById("newCompra");

            NewCompra.addEventListener("click", function(){
                document.getElementById('divNuevaCompra').style.display = 'block';
            });
        });

        let banderaHide = 0;
    $("#divNuevaCompra").hide('slow'); 
     
    $('#newCompra').on('click', function(){
        if(banderaHide==0){
            $("#divNuevaCompra").show('slow'); 
            banderaHide = 1;
        }else{
            $("#divNuevaCompra").hide('slow'); 
            banderaHide = 0;
        }
    });

    function editarC(id_compra,id_proveedor,id_venta,id_cuenta_pagar,id_cuenta_cobrar,credito_fiscal,monto_com, concepto_com, fecha_emision, fecha_vencimiento, sub_total){

            $('#uid_compra').val(id_compra); 
            $('#uid_proveedor').val(id_proveedor); 
            $('#uid_venta').val(id_venta);
            $('#uid_cuenta_pagar').val(id_cuenta_pagar);
            $('#uid_cuenta_cobrar').val(id_cuenta_cobrar);
            $('#ucredito_fiscal').val(credito_fiscal);
            $('#umonto_com').val(monto_com);
            $('#uconcepto_com').val(concepto_com);
            $('#ufecha_emision').val(fecha_emision);
            $('#ufecha_vencimiento').val(fecha_vencimiento);   
            $('#usub_total').val(sub_total);   

            $('#compraModalEdit').modal();
        }

        function eliminarC(id_compra){
            $('#did_compra').val(id_compra); 
            $('#compraModalDelete').modal();
            
        }

</script>

@endpush