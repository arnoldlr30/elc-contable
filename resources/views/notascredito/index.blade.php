@extends('layouts.main', ['activePage' => 'notas-credito', 'titlePage' => __('Notas de Crédito')])
@section('title', 'Notas de Crédito')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }    
</style>

@include('notascredito.notas-list')
@include('notascredito.update.nota-delete')

@endsection

@push('js')

<script>
   
   function eliminarN(id_nt){
            $('#dId_nt').val(id_nt); 
            $('#notaModalDelete').modal();  
        }
    
</script>

<script src="{{asset('assets/js/maestroCliente.js')}}"></script>
<script src="{{asset('assets/js/validaciones/vCliente.js')}}"></script>

@endpush