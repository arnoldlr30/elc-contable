@extends('layouts.main', ['activePage' => 'notas-credito', 'titlePage' => __('Notas de Crédito')])
@section('title', 'Notas de Crédito')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }    
</style>


@include('notascredito.update.form')

@endsection

@push('js')

<script>
   
    
</script>

<script src="{{asset('assets/js/maestroCliente.js')}}"></script>
<script src="{{asset('assets/js/validaciones/vCliente.js')}}"></script>

@endpush