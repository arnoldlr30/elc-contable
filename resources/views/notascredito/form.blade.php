<form action="{{route('nota-credito.store')}}" method="POST" id="frm">
    @csrf
    <div class="row container p-5">
        <div class="col-md-12">
            <center><h3>Nueva nota de crédito</h3></center>
        </div>
        <div class="col-md-6 mt-3">
            <label>#NCR<b>*</b></label>
            <input type="number" value="{{old('')}}"  class="txt-form" name="ncr">
            @error('ncr')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Concepto<b>*</b></label>
            <input type="text" value="{{old('')}}"  class="txt-form" name="concepto">
            @error('concepto')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Monto<b>*</b></label>
            <input type="number" class="txt-form" name="monto" >
            @error('monto')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Fecha emisión<b>*</b></label>
            <input type="date" class="txt-form" name="fecha_emision" >
            @error('fecha_emision')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>#Factura <b>*</b></label>
            <select class="livesearch form-control select-css p-3" name="id_factura" id="livesearch">
                    
                </select>
            {{-- <select class="select-css" name="id_factura" >
                    <option value="">Seleccione una factura</option>
                    @foreach ($ventas as $venta)
                        <option value="{{$venta->id_venta}}" {{ old('id_venta') == $venta->id_venta ? 'selected' : '' }}>{{$venta->credito_fiscal}} - {{$venta->cliente}}</option>
                    @endforeach
                </select> --}}
                @error('id_venta')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Cliente <b>*</b></label>
            <select class="livesearch2 form-control select-css p-3" name="id_cliente" id="livesearch2">
                    
                </select>
           {{--  <select class="select-css" name="id_cliente">
                    <option value="">Seleccione un cliente</option>
                    @foreach ($clientes as $c)
                        <option value="{{$c->id_cliente}}" {{ old('id_cliente') == $c->id_cliente ? 'selected' : '' }}>{{$c->nombre_cliente}}</option>
                    @endforeach
                </select> --}}
                @error('id_cliente')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
        </div>
         <div class="col-3 mt-3">
                <input type="radio"  name="anulada" value="Si" {{ old('anulada')=="Anulada" ? 'checked='.'"'.'checked'.'"' : '' }}>
                <label for="Pagada">Anular factura</label><br>
                </div>
        <div class="row justify-content-center">
            <input type="submit" value="Guardar" class=" mt-5 btn btn-radius btn-azul">
        </div>
        <hr>                   
</div>
<br>
<hr>

</form>   

<script type="text/javascript">
    $('.livesearch').select2({
        placeholder: 'Seleccione una factura',
        
        ajax: {
            url: '/ajax-autocomplete-search-venta',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.credito_fiscal,
                            id: item.id_venta
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>

<script type="text/javascript">
    $('.livesearch2').select2({
        placeholder: 'Seleccione un cliente',
        
        ajax: {
            url: '/ajax-autocomplete-search',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nombre_cliente,
                            id: item.id_cliente
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>