@extends('layouts.main', ['activePage' => 'notas-credito', 'titlePage' => __('Notas de Crédito')])
@section('title', 'Notas de Crédito')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }    
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

@include('notascredito.form')

@endsection

@push('js')

<script>
   
    
</script>

<script src="{{asset('assets/js/maestroCliente.js')}}"></script>
<script src="{{asset('assets/js/validaciones/vCliente.js')}}"></script>

@endpush