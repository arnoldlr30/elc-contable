<div class="container p-5">
  <div class="">
      <center><h3>Notas de Crédito</h3></center>
      <a href="{{ Route('new-nota-credito') }}" type="button" class="btn btn-info">Nueva nota</a>
  </div>
<table class="table mt-4">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Factura</th>
      <th scope="col">Concepto</th>
      <th scope="col">Monto</th>
      <th scope="col">Cliente</th>
      <th scope="col">Fecha emisión</th>
      <th scope="col"><center><i class="fas fa-cog"></i></center></th>
    </tr>
  </thead>
  <tbody>
    @foreach ($notas as $nota)
    <tr>
      <th scope="row">{{$nota->id_nt}}</th>
      <td>{{$nota->factura}}</td>
      <td>{{$nota->concepto}}</td>
      <td>{{$nota->monto}}</td>
      <td>{{$nota->cliente}}</td>
      <td>{{$nota->fecha_emision->format('d/m/Y')}}</td>
      <td>
        <a href="{{Route('nota.edit-form', $nota->id_nt)}}"><i class="fas fa-edit p-2"></i></a>
        <a type="image" onclick="eliminarN({{$nota->id_nt}});"  class="btn-calc math sombra"><i class="fas fa-trash-alt p-2" style="cursor: pointer; width: 30px; height: 30px;"></i></a>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>