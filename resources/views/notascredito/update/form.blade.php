<form action="{{route('nota-credito.edit', $nota->id_nt)}}" method="POST" id="frm">
    @csrf
    <div class="row container p-5">
        <div class="col-md-12">
            <center><h3>Editar nota de crédito</h3></center>
        </div>
        <div class="col-md-6 mt-3">
            <label>#NCR<b>*</b></label>
            <input type="number" value="{{$nota->ncr}}"  class="txt-form" name="ncr">
            @error('ncr')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Concepto<b>*</b></label>
            <input type="text" value="{{$nota->concepto}}" class="txt-form" name="concepto">
            @error('concepto')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Monto<b>*</b></label>
            <input type="number" value="{{$nota->monto}}" class="txt-form" name="monto" >
            @error('monto')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Fecha emisión<b>*</b></label>
            <input type="date" value="{{ $nota->fecha_emision->format('Y-m-d') }}" class="txt-form" name="fecha_emision" >
            @error('fecha_emision')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>#Factura <b>*</b></label>
            <select class="select-css" name="id_factura" >
                    <option value="{{$nota->id_factura}}">{{$nota->factura}}</option>
                    @foreach ($ventas as $venta)
                        <option value="{{$venta->id_venta}}" {{ old('id_venta') == $venta->id_venta ? 'selected' : '' }}>{{$venta->credito_fiscal}} - {{$venta->cliente}}</option>
                    @endforeach
                </select>
                @error('id_venta')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
        </div>
        <div class="col-md-6 mt-3">
            <label>Cliente <b>*</b></label>
            <select class="select-css" name="id_cliente">
                    <option value="{{$nota->id_cliente}}">{{$nota->cliente}}</option>
                    @foreach ($clientes as $c)
                        <option value="{{$c->id_cliente}}" {{ old('id_cliente') == $c->id_cliente ? 'selected' : '' }}>{{$c->nombre_cliente}}</option>
                    @endforeach
                </select>
                @error('id_cliente')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
        </div>
         <div class="col-3 mt-3">
                <input type="radio" name="anulada" value="Si" {{ $nota->anulada == 'Si' ? 'checked='.'"'.'checked'.'"' : '' }}>
                <label for="Pagada">Anulada</label><br>
                </div>
        <div class="row justify-content-center">
            <input type="submit" value="Guardar" class=" mt-5 btn btn-radius btn-azul">
        </div>
        <hr>                   
</div>
<br>
<hr>

</form>   