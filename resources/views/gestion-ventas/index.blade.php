@extends('layouts.main', ['activePage' => 'gestion-ventas', 'titlePage' => __('Gestión de ventas')])
@section('title', 'Gestión Ventas')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }    
</style>


@include('gestion-ventas.ventas-list')

@endsection

@push('js')

<script>
   
    
</script>

<script src="{{asset('assets/js/maestroCliente.js')}}"></script>
<script src="{{asset('assets/js/validaciones/vCliente.js')}}"></script>

@endpush