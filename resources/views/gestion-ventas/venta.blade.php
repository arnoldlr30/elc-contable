@extends('layouts.main', ['activePage' => 'gestion-ventas', 'titlePage' => __('Nueva venta')])
@section('title', 'Compra')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }    
</style>


@include('gestion-ventas.form')

@endsection

@push('js')

<script>
   
    
</script>

<script src="{{asset('assets/js/maestroCliente.js')}}"></script>
<script src="{{asset('assets/js/validaciones/vCliente.js')}}"></script>

@endpush