<form action="{{route('maestroCliente.store')}}" method="POST" id="frm">
    @csrf
    <div class="row container p-5">
        <div class="col-md-12">
            <center><h3>Nueva venta</h3></center>
        </div>
        <div class="col-md-6 mt-3">
            <label>Orden de compra<b>*</b></label>
            <input type="number" value="{{old('')}}"  class="txt-form" name="" id="">
            @error('')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-12 mt-3">
            <label>Factura<b>*</b></label>
            <div id="nombre_del_sujeto">
                <input type="radio"    name="nombre_del_sujeto" value="Natural"  {{ old('nombre_del_sujeto')=="Natural" ? 'checked='.'"'.'checked'.'"' : '' }}>
                <label for="Natural">Pagada</label><br>
                <input type="radio"    name="nombre_del_sujeto" value="Natural"  {{ old('nombre_del_sujeto')=="Natural" ? 'checked='.'"'.'checked'.'"' : '' }}>
                <label for="Natural">Anulada</label><br>
            </div>
            @error('nombre_del_sujeto')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-12 mt-3">
            
            <div id="nombre_del_sujeto">
                <input type="radio"    name="nombre_del_sujeto" value="Natural"  {{ old('nombre_del_sujeto')=="Natural" ? 'checked='.'"'.'checked'.'"' : '' }}>
                <label>Pagar después</label><br>
            </div>
            @error('nombre_del_sujeto')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <hr>                   
</div>
<br>
<hr>

</form>   