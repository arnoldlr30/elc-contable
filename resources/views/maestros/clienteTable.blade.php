<table style="border-radius: 10px;" id="maestroClienteTable"  class="w-100 align-items-center table table-responsive table-sm table-hover">
    <thead class="thead-dark">
        <tr>
             <th>#</th>
            
             <th>Nombre comercial</th>
             <th>Nombre del sujeto</th>
             
             
             <th>Ciudad</th>
            
             <th>Municipio</th>
            
             <th>Giro fical de negocio</th>
             <th>Tipo contribuyente</th>
            
             <th>-</th>
             <th>-</th>
             
        </tr>
    </thead>
    <tbody>
        @foreach ($maestro as $us)
         <tr>
             <td>{{$us->id_maestro_cliente}}</td>
            
             <td><a href="{{Route('cliente.datos', $us->id_maestro_cliente)}}">{{$us->nombre_comercial}}</a></td>
            
             <td>{{$us->nombre_del_sujeto}}</td>
             
             <td>{{$us->ciudad}}</td>
             <td>{{$us->municipio}}</td>
             
             <td>{{$us->giro_fical_negocio}}</td>
             <td>{{$us->tipo_contribuyente}}</td>
             <td>
                <a onclick="eliminarM({{$us->id_maestro_cliente}},{{$us->id_cliente}});" form="formulario1" height="40px" width="40px" 
                ><i class="fas fa-trash-alt p-2" style="cursor: pointer; width: 30px; height: 30px;"></i></a>
             </td>
             <td>
                 <a href="{{Route('cliente.form-update', $us->id_maestro_cliente)}}"><i class="fas fa-edit p-2"></i></a>
            </td>
             
         </tr>    
         @endforeach
    </tbody>
</table>