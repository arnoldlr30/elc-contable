<div class="container mt-5 p-5">

	<div class="row">

		<div class="col-12">
		<h3>Datos generales del Cliente</h3>
		
		<table class="table mt-4">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Nombre</th>
			      <th scope="col">N° Cliente ICG</th>
			      <th scope="col">Otro</th>
			      <th scope="col">Nombre Comercial</th>
			      <th scope="col">Sujeto</th>
			      <th scope="col">P. Fiscal</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{{$maestro[0]->nombre_cliente}}</td>
			      <td>{{$maestro[0]->numero_cliente_icg}}</td>
			      <td>{{$maestro[0]->numero_cliente}}</td>
			      <td>{{$maestro[0]->nombre_comercial}}</td>
			      <td>{{$maestro[0]->nombre_del_sujeto}}</td>
			      <td>{{$maestro[0]->paraiso_fiscal}}</td>
			    </tr>
			  </tbody>
		</table>

		<h3>Dirrección</h3>
		
		<table class="table mt-4">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Dirección</th>
			      <th scope="col">País</th>
			      <th scope="col">Código país</th>
			      <th scope="col">Ciudad</th>
			      <th scope="col">Departamento</th>
			      <th scope="col">Municipio</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{{$maestro[0]->direccion}}</td>
			      <td>{{$maestro[0]->pais}}</td>
			      <td>{{$maestro[0]->codigo_pais}}</td>
			      <td>{{$maestro[0]->ciudad}}</td>
			      <td>{{$maestro[0]->departamento}}</td>
			      <td>{{$maestro[0]->municipio}}</td>
			    </tr>
			  </tbody>
		</table>
		<h3>Contacto</h3>
		
		<table class="table mt-4">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Teléfono</th>
			      <th scope="col">Página web</th>
			      <th scope="col">Correo</th>
			      <th scope="col">Celular</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{{$maestro[0]->telefono_fijo}}</td>
			      <td>{{$maestro[0]->pagina_web}}</td>
			      <td>{{$maestro[0]->correo}}</td>
			      <td>{{$maestro[0]->telefono_celular}}</td>
			      
			    </tr>
			  </tbody>
		</table>
		<h3>Persona de contacto</h3>
		
		<table class="table mt-4">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Nombre</th>
			      <th scope="col">Cargo</th>
			      <th scope="col">Página web</th>
			      <th scope="col">Correo</th>
			      <th scope="col">Teléfono</th>
			      
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{{$maestro[0]->nombre_contacto}}</td>
			      <td>{{$maestro[0]->correo_contacto}}</td>
			      <td>{{$maestro[0]->pagina_web_contacto}}</td>
			      <td>{{$maestro[0]->correo_contacto}}</td>
			      <td>{{$maestro[0]->telefono_contacto}}</td>
			      
			    </tr>
			  </tbody>
		</table>
		<h3>Información general</h3>
		
		<table class="table mt-4">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Moneda</th>
			      <th scope="col">Tipo Cambio</th>
			      <th scope="col">Giro fiscal</th>
			      <th scope="col">Contribuyente</th>
			      <th scope="col">NIT/NIFF</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{{$maestro[0]->moneda_principal}}</td>
			      <td>{{$maestro[0]->tipo_cambio}}</td>
			      <td>{{$maestro[0]->giro_fical_negocio}}</td>
			      <td>{{$maestro[0]->tipo_contribuyente}}</td>
			      <td>{{$maestro[0]->nit_niff}}</td>
			      
			    </tr>
			  </tbody>
		</table>
		<table class="table mt-4">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">N° Registro fiscal</th>
			      <th scope="col">Cobra iva</th>
			      <th scope="col">Entera iva</th>
			      <th scope="col">Emitirá N/C</th>
			      <th scope="col">Retención(%)</th>
			      <th scope="col">Percepción</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{{$maestro[0]->n_registro_fiscal}}</td>
			      <td>{{$maestro[0]->cobra_iva}}</td>
			      <td>{{$maestro[0]->entera_iva}}</td>
			      <td>{{$maestro[0]->emitira_nc}}</td>
			      <td>%{{$maestro[0]->porc_retencion}}</td>
			      <td>{{$maestro[0]->percepcion}}</td>
			      
			    </tr>
			  </tbody>
		</table>
		<table class="table mt-4">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Cta Pasivo #1</th>
			      <th scope="col">Cta Pasivo #2</th>
			      <th scope="col">Cta Activo #1</th>
			      <th scope="col">Cta Activo #2</th>
			      <th scope="col">Comisión(%)</th>
			      <th scope="col">Cond. Ope</th>
			      <th scope="col">Cond. Cre</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <td>{{$maestro[0]->cta_pasivo_uno}}</td>
			      <td>{{$maestro[0]->cta_pasivo_dos}}</td>
			      <td>{{$maestro[0]->cta_activo_uno}}</td>
			      <td>{{$maestro[0]->cta_activo_dos}}</td>
			      <td>%{{$maestro[0]->comision}}</td>
			      <td>{{$maestro[0]->condiciones_operacion}}</td>
			      <td>{{$maestro[0]->condiciones_credito}}</td>
			      
			    </tr>
			  </tbody>
		</table>
		</div>
		
	</div>
	
</div>