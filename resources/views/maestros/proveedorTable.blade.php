<table style="border-radius: 10px;" id="maestroProveedorTable"  class="w-100 align-items-center table table-responsive table-sm table-hover">
    <thead class="thead-dark">
        <tr>
             <th>#</th>
             
             <th>Nombre comercial</th>
             <th>Nombre del sujeto</th>
             <th>Ciudad</th>
             
             <th>Municipio</th>
             <th>Giro fical de negocio</th>
             <th>Tipo contribuyente</th>
             <th>-</th>
             <th>-</th>
             
        </tr>
    </thead>
    <tbody>
        @foreach ($maestro as $us)
         <tr>
             <td>{{$us->id_maestro_proveedor}}</td>
             <td><a href="{{Route('proveedor.datos', $us->id_maestro_proveedor)}}">{{$us->nombre_comercial}}</a></td>
             
             <td>{{$us->nombre_del_sujeto}}</td>
             
             <td>{{$us->ciudad}}</td>
             
             <td>{{$us->municipio}}</td>
             
             <td>{{$us->giro_fical_negocio}}</td>
             <td>{{$us->tipo_contribuyente}}</td>
             
             <td>
                <a onclick="eliminarM({{$us->id_maestro_proveedor}},{{$us->id_proveedor}});"><i class="fas fa-trash-alt p-2" style="cursor: pointer; width: 30px; height: 30px;"></i><a/>
             </td>
             <td>
                <a href="{{Route('proveedor.form-update', $us->id_maestro_proveedor)}}"><i class="fas fa-edit p-2"></i></a>
            </td>
             
             
         </tr>    
         @endforeach
    </tbody>
</table>