@if($ven->estado_venta == 'Quedan')
<TH style="background-color: #F9D148; border-radius: 5px; color: white;">
    <button class="btn" onclick="pagarV({{$ven->id_venta}})" style="background-color: #F9D148;">
        Aún no ha sido cobrada
    </button>
</TH>
@elseif($ven->estado_venta == 'Pendiente')
<TH style="background-color: #F9D148; border-radius: 5px; color: white;">
    <button class="btn" onclick="pagarV({{$ven->id_venta}})" style="background-color: #F9D148;">
        Aún no ha sido cobrada
    </button>
</TH>
@elseif($ven->estado_venta == 'Anulada')
<TH style="background-color: #FD3228; border-radius: 5px; color: white;">
    <button class="btn" onclick="pagarV({{$ven->id_venta}})" style="background-color: #FD3228;">
       Anulada
    </button>
</TH>
@endif