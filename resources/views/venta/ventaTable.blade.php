<table style="border-radius: 10px; text-align: center"  id="ventaTable"  class="align-items-center table table-responsive table-sm table-hover">
    <thead class="thead-dark">
        <tr>
             <th>#</th>
             <th>Cliente</th>
             <th>Crédito Fiscal</th>
             <th>Monto de la Venta</th>
             <th>Concepto Venta</th>
             <th>Fecha en que se pagó</th>
             <th>-</th>
             <th>-</th>
             <th>-</th>
             <th>-</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ventas as $ven)
         <tr>
             <td>{{$ven->id_venta}}</td>
             <td>{{$ven->nombre_cliente}}</td>
             <td>{{$ven->credito_fiscal}}</td>
             <th>${{number_format($ven->monto_ven, 2, ".",",")}}</th>
             <td>{{$ven->concepto_ven}}</td>
             {{-- <td>{{$ven->fecha_emision->format('d/m/Y')}}</td>
             <td>{{$ven->fecha_vencimiento->format('d/m/Y')}}</th> --}}
             @if($ven->estado_venta == 'Pagada')
                <th style="background-color: #81b214;">
                    <button class="btn" style="background-color: #81b214;" onclick="editarP({{$ven->id_venta}}, {{$ven->fecha_pago->format('d/m/Y')}})">Fue cobrada: 
                        {{$ven->fecha_pago->format('d/m/Y')}}
                    </button>
                </th>
             @else
                @include('venta.btnCobrar')
             @endif
             <td>
                <a type="image" onclick="eluminarV({{$ven->id_venta}});"  class="btn-calc math sombra"><i class="fas fa-trash-alt p-2" style="cursor: pointer; width: 30px; height: 30px;"></i></a>
             </td>
             <td>
                 <a href="{{Route('venta.edit-form', $ven->id_venta)}}"><i class="fas fa-edit p-2" style="cursor: pointer;"></i></a>
            </td>
            <td>
                <a href="{{route('detalleVenta.index', $ven->id_venta)}}">Detalles</a>
            </td>
            <td>
                <a href="{{route('venta.compras', $ven->id_venta)}}">Compras</a>
            </td>
             
         </tr>    
         @endforeach
    </tbody>
</table>