@extends('layouts.main', ['activePage' => 'venta', 'titlePage' => __('Venta')])
@section('title', 'Nueva Venta ELC')
@section('content')
<style>
    b{
        color: red;
    }

    small{
        color: red;
    }

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    
<div>
    <x-table>
        
        <div class="row justify-content-center pt-5">
            <h3>Nueva Venta</h3>
        </div>
        <div class="m-5 my-2">
            
            @include('venta.update.update-venta')   
            
        </div>
    </x-table>
</div>

@endsection

@push('js')

    @if ($errors->any())
        <script>
            toastr["error"]("Hay algun error con los campos que usted ingresó", "Algo salió mal");
        </script>
    @endif

    

   <script>

       $('#usuarioTable').DataTable({
             
             responsive: true,
             autowidth: false,
     
             "language": {
                 "lengthMenu": "Mostrar _MENU_ registros por página",
                 "zeroRecords": "Nada encontrado - disculpa :(",
                 "info": "Mostrando la página _PAGE_ de _PAGES_",
                 "infoEmpty": "No hay registros disponibles",
                 "infoFiltered": "(filtrado de _MAX_ registros totales)",
                 "search": "Buscar:",
                 "paginate": {
                     "next": "Siguiente",
                     "previous": "Anterior",
                 }
             }
                   
         });

         

    
   </script>


@endpush
