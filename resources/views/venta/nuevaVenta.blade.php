<form action="{{route('venta.store')}}" method="POST">
    @csrf
    <div class="">
        <div class="row mt-4 p-4">
            <div class="col-4 mb-3">
                <label>Cliente</label><br>
                {{-- <select id="" class="select-css" name="id_cliente">
                    <option value="">Seleccione un cliente</option>
                    @foreach ($clientes as $c)
                        <option value="{{$c->id_cliente}}" {{ old('id_cliente') == $c->id_cliente ? 'selected' : '' }}>{{$c->nombre_cliente}}</option>
                    @endforeach
                </select> --}}
                <select class="livesearch form-control select-css p-3" name="id_cliente" id="livesearch">
                     
                </select>
                @error('id_cliente')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-4 mb-3">
                <label>Crédito Fiscal</label>
                <input type="text" class="txt-form" name="credito_fiscal" value="{{old('credito_fiscal')}}">
                @error('credito_fiscal')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-4 mb-3">
                <label>Monto Venta</label>
                <input type="number" class="txt-form" step="0.01" min="1" name="monto_ven" value="{{old('monto_ven')}}">
                @error('monto_ven')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-4 mb-3">
                <label>Concepto Venta</label>
                <input type="text" class="txt-form" maxlength="50" name="concepto_ven" value="{{old('concepto_ven')}}">
                @error('concepto_ven')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-md-4">
            <label>Orden de compra</label>
            <input type="text" class="txt-form" name="orden_compra">
            @error('')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-4">
            <label>Presupuesto</label>
            <input type="text" class="txt-form" name="presupuesto">
            @error('')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-md-4">
            <label>Mercaderia</label>
            <input type="text" class="txt-form" name="mercaderia">
            @error('')
            <small>*{{$message}}</small>
            @enderror
        </div>
            <div class="col-4">
                <label>Fecha Emisión</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_emision" value="{{old('fecha_emision')}}">
                @error('fecha_emision')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-4">
                <label>Fecha Vencimiento</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_vencimiento" value="{{old('fecha_vencimiento')}}">
                @error('fecha_vencimiento')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-md-12 mt-3">
            <label>Factura</label><br>
            <div class="row">
                <div class="col-3">
                <input type="radio"  name="estado_venta" value="Anulada" {{ old('estado_venta')=="Anulada" ? 'checked='.'"'.'checked'.'"' : '' }} onchange="mostrar(this.value);">
                <label for="Pagada">Anulada</label><br>
                </div>
                <div class="col-3">
                <input type="radio"  name="estado_venta" value="Pagada" {{ old('estado_venta')=="Pagada" ? 'checked='.'"'.'checked'.'"' : '' }} onchange="mostrar(this.value);">
                <label for="Pagada">Pagada</label><br>
                </div> 
                <div class="col-3">
                <input type="radio" value="Quedan" {{ old('estado_venta')=="Quedan" ? 'checked='.'"'.'checked'.'"' : '' }} onchange="mostrar(this.value);" name="estado_venta">
                <label>Quedan</label><br>
            </div>
            </div>
            @error('Anulada')
            <small>*{{$message}}</small>
            @enderror
        </div>
        <div class="col-12 mt-3" id="datos" style="display: none;">
            <div class="row">
             <div class="col-4">
                <label>Fecha</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_pago" value="{{old('fecha_pago')}}">
                @error('fecha_pago')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-4">
            <label>N. de cheque</label>
            <input type="number" class="txt-form" name="num_cheque">
            </div>
            <div class="col-4">
            <label>N. Mandamiento</label>
            <input type="number" class="txt-form" name="num_mandamiento">
            </div>
            </div>
        </div>
         <div class="col-4 mt-3" id="fecha_quedan" style="display: none;">
                <label>Fecha</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_quedan" value="{{old('fecha_quedan')}}">
                @error('fecha_quedan')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
        </div>
        <div class="row justify-content-center">
            <input type="submit" value="Agregar Venta" class=" mt-3 btn btn-radius btn-azul">
        </div>
        
    </div>

</form>


@if ($errors->any())
    <script>
        document.getElementById('divNuevaVenta').style.display = 'block';
    </script>
@else
    <script>document.getElementById('divNuevaVenta').style.display = 'none';</script>  
@endif

<script>
function mostrar(dato) {
  if (dato == "Pagada") {
    document.getElementById("datos").style.display = "block";
    document.getElementById('fecha_quedan').style.display = 'none';
  }
  if (dato == "Quedan") {
    document.getElementById("fecha_quedan").style.display = "block";
    document.getElementById("datos").style.display = "none";
  }if (dato == 'Anulada'){
    document.getElementById("fecha_quedan").style.display = "none";
    document.getElementById("datos").style.display = "none";
  }
}
</script>

<script type="text/javascript">
    $('.livesearch').select2({
        placeholder: 'Seleccione un cliente',
        
        ajax: {
            url: '/ajax-autocomplete-search',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nombre_cliente,
                            id: item.id_cliente
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
    
