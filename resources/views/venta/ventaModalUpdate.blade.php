<!-- Modal -->
<div class="modal fade " id="ventaModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Actualizar datos del usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('venta.update')}}" autocomplete="of" method="POST">
                
                @csrf
                @method('PUT')
                <!--Hidden inputs-->
                <input type="hidden" name="uId_venta" id="uId_venta">
                <div class="container">

                    <div class="row">
                        <div class="col-6">
                            <label>Cliente</label>
                            <select id="uId_cliente" class="select-css" name="uId_cliente" >
                                <option value="">Seleccione un cliente</option>
                                @foreach ($clientes as $c)
                                    <option value="{{$c->id_cliente}}" {{ old('uId_cliente') == $c->id_cliente ? 'selected' : '' }}>{{$c->nombre_cliente}}</option>
                                @endforeach
                            </select>
                            @error('uId_cliente')
                                    <small>*{{$message}}</small>
                                <br>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label>Crédito Fiscal</label>
                            <input type="text" class="txt-form" name="uCredito_fiscal" id="uCredito_fiscal" value="{{old('uCredito_fiscal')}}">
                            @error('uCredito_fiscal')
                                    <small>*{{$message}}</small>
                                <br>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label>Monto Venta</label>
                            <input type="number" class="txt-form" step="0.01" min="1" name="uMonto_ven" id="uMonto_ven" value="{{old('monto_ven')}}">
                            @error('uMonto_ven')
                                    <small>*{{$message}}</small>
                                <br>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label>Concepto Venta</label>
                            <input type="text" class="txt-form" maxlength="50" name="uConcepto_ven" id="uConcepto_ven" value="{{old('uConcepto_ven')}}">
                            @error('uConcepto_ven')
                                    <small>*{{$message}}</small>
                                <br>
                            @enderror
                        </div>
                        <div class="col-md-4">
                        <label>Orden de compra</label>
                        <input type="text" class="txt-form" value="{{old('uOrden_compra')}}" id="uOrden_compra" name="uOrden_compra">
                        @error('uOrden_compra')
                        <small>*{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-md-4">
                        <label>Presupuesto</label>
                        <input type="text" class="txt-form" id="uPresupuesto" name="uPresupuesto" value="{{old('uPresupuesto')}}">
                        @error('uPresupuesto')
                        <small>*{{$message}}</small>
                        @enderror
                    </div>
                    <div class="col-md-4">
                        <label>Mercaderia</label>
                        <input type="text" class="txt-form" id="uMercaderia" name="uMercaderia" value="{{old('uMercaderia')}}">
                        @error('uMercaderia')
                        <small>*{{$message}}</small>
                        @enderror
                    </div>
                        <div class="col-6">
                            <label>Fecha Emisión</label>
                            <input type="date" class="txt-form" maxlength="50" name="uFecha_emision" id="uFecha_emision" value="{{old('uFecha_emision')}}">
                            @error('uFecha_emision')
                                    <small>*{{$message}}</small>
                                <br>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label>Fecha Vencimiento</label>
                            <input type="date" class="txt-form" maxlength="50" name="uFecha_vencimiento" id="uFecha_vencimiento"  value="{{old('uFecha_vencimiento')}}">
                            @error('uFecha_vencimiento')
                                    <small>*{{$message}}</small>
                                <br>
                            @enderror
                        </div>
                        <div class="col-md-12 mt-3">
            <label>Factura</label><br>
            <div class="row">
                <div class="col-3">
                <input type="radio"  name="uEstado_venta" value="Anulada" {{ old('estado_venta')=="Anulada" ? 'checked='.'"'.'checked'.'"' : '' }} onchange="mostrar(this.value);">
                <label for="Pagada">Anulada</label><br>
                </div>
                <div class="col-3">
                <input type="radio"  name="estado_venta" value="Pagada" {{ old('estado_venta')=="Pagada" ? 'checked='.'"'.'checked'.'"' : '' }} onchange="mostrar(this.value);">
                <label for="Pagada">Pagada</label><br>
                </div> 
                <div class="col-3">
                <input type="radio" value="Quedan" {{ old('estado_venta')=="Quedan" ? 'checked='.'"'.'checked'.'"' : '' }} onchange="mostrar(this.value);" name="estado_venta">
                <label>Quedan</label><br>
            </div>
            </div>
            @error('Anulada')
            <small>*{{$message}}</small>
            @enderror
        </div>
         <div class="col-12 mt-3" id="datos" style="display: none;">
            <div class="row">
             <div class="col-4">
                <label>Fecha</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_pago" value="{{old('fecha_pago')}}">
                @error('fecha_pago')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
            <div class="col-4">
            <label>N. de cheque</label>
            <input type="number" class="txt-form" name="num_cheque">
            </div>
            <div class="col-4">
            <label>N. Mandamiento</label>
            <input type="number" class="txt-form" name="num_mandamiento">
            </div>
            </div>
        </div>
         <div class="col-4 mt-3" id="fecha_quedan" style="display: none;">
                <label>Fecha</label>
                <input type="date" class="txt-form" maxlength="50" name="fecha_quedan" value="{{old('fecha_quedan')}}">
                @error('fecha_quedan')
                        <small>*{{$message}}</small>
                    <br>
                @enderror
            </div>
                    </div>

                </div>
                
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger">Actualizar</button>
                </div>
            </form>

        </div>
    </div>

</div>
@if ($errors->any())
    <script>
        document.getElementById('divNuevaVenta').style.display = 'block';
    </script>
@else
    <script>document.getElementById('divNuevaVenta').style.display = 'none';</script>  
@endif

<script>
function mostrar(dato) {
  if (dato == "Pagada") {
    document.getElementById("datos").style.display = "block";
    document.getElementById('fecha_quedan').style.display = 'none';
  }
  if (dato == "Quedan") {
    document.getElementById("fecha_quedan").style.display = "block";
    document.getElementById("datos").style.display = "none";
  }if (dato == 'Anulada'){
    document.getElementById("fecha_quedan").style.display = "none";
    document.getElementById("datos").style.display = "none";
  }
}
</script>

<script type="text/javascript">
    $('.livesearch').select2({
        placeholder: 'Seleccione un cliente',
        
        ajax: {
            url: '/ajax-autocomplete-search',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nombre_cliente,
                            id: item.id_cliente
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
    
