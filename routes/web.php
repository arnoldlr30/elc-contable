<?php

use App\Http\Controllers\AdministradorController;
use App\Http\Controllers\MaestroClienteController;
use App\Http\Controllers\MaestroProveedorController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\DetalleVentaController;
use App\Http\Controllers\VentaController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\CompraController;
use App\Http\Controllers\CuentasPagarController;
use App\Http\Controllers\CuentasCobrarController;
use App\Http\Controllers\DetalleCompraController;
use App\Http\Controllers\NotasCreditoController;
use App\Models\CotCatalogoCredito;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

use App\Http\Livewire\ShowUsers;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Notas de credito Routes***************************************************/

Route::get('/notas-credito', [NotasCreditoController::class, 'index'])->name('notas-credito.index');

Route::get('/new-nota-credito', [NotasCreditoController::class, 'nota'])->name('new-nota-credito');

Route::post('/new-nota-credito', [NotasCreditoController::class, 'store'])->name('nota-credito.store');

Route::get('/nota-credito/edit/{id}', [NotasCreditoController::class, 'edit_form'])->name('nota.edit-form');

Route::post('/nota-credito/edit/{id}', [NotasCreditoController::class, 'edit'])->name('nota-credito.edit');

Route::delete('/nota-credito/delete', [NotasCreditoController::class, 'delete'])->name('nota.delete');
Route::delete('nota-credito/destroy', [NotasCreditoController::class, 'destroy'])->name('nota.destroy');

//*****************************************************************************

/* Compras Routes*/

Route::get('/compras-list', [CompraController::class, 'index'])->name('compras.list');

Route::get('/new-compra', [CompraController::class, 'Addcompra'])->name('new-compra');

Route::post('/new-compra', [CompraController::class, 'storeCompra'])->name('new-compra.store');

Route::post('/compra/create/', [CompraController::class, 'store'])->name('compra.create');

Route::get('/ajax-autocomplete-search-venta', [CompraController::class, 'selectSearch2']);

Route::get('/ajax-autocomplete-search-cxp', [CompraController::class, 'selectSearch3']);

Route::get('/ajax-autocomplete-search-cxc', [CompraController::class, 'selectSearch4']);

Route::get('/compra/edit/{id}', [CompraController::class, 'edit_form'])->name('compra.edit-form');

Route::post('/compra/edit', [CompraController::class, 'update'])->name('compra.edit');

/* gestion de ventas Routes*/

Route::get('/gestion-ventas', [VentaController::class, 'ventas'])->name('gestion-ventas.index');

Route::get('/new-venta', [VentaController::class, 'Addventa'])->name('new-venta');

Route::get('/ajax-autocomplete-search', [VentaController::class, 'selectSearch']);

Route::get('/venta/compras/{id}', [VentaController::class, 'venta_compras'])->name('venta.compras');


/* Maestro Cliente Routes*/
Route::get('/maestro-cliente', [MaestroClienteController::class, 'index'])->name('maestroCliente.index');
Route::post('/maestro-cliente/store', [MaestroClienteController::class, 'store'])->name('maestroCliente.store');
Route::put('/maestro-cliente/update', [MaestroClienteController::class, 'update'])->name('maestroCliente.update');
Route::delete('/maestro-cliente/destroy', [MaestroClienteController::class, 'destroy'])->name('maestroCliente.delete');
Route::get('/maestro-cliente/update/{id}', [MaestroClienteController::class, 'update_form'])->name('cliente.form-update');

Route::post('maestro-cliente/editar', [MaestroClienteController::class, 'update'])->name('cliente.editar');
Route::get('/maestro-cliente/detalles/{id}', [MaestroClienteController::class, 'cliente'])->name('cliente.datos');

/*Ajax Maestros*/
Route::get('/estados', [MaestroClienteController::class, 'getEstados']);
Route::get('/municipios', [MaestroClienteController::class, 'getMunicipios']);
Route::get('/pariso-pais', [MaestroClienteController::class, 'getParaiso']);
Route::get('/pariso-estado', [MaestroClienteController::class, 'getParaisoEstado']);
Route::get('/codigo', [PaisController::class, 'getCodigoPais']);
Route::get('/codigoPais', [PaisController::class, 'getCodigo']);


/* Maestro Proveedor Routes*/
Route::get('/maestro-proveedor', [MaestroProveedorController::class, 'index'])->name('maestroProveedor.index');
Route::post('/maestro-proveedor/store', [MaestroProveedorController::class, 'store'])->name('maestroProveedor.store');
Route::put('/maestro-proveedor/update', [MaestroProveedorController::class, 'update'])->name('maestroProveedor.update');
Route::delete('/maestro-proveedor/destroy', [MaestroProveedorController::class, 'destroy'])->name('maestroProveedor.delete');

Route::get('/autocomplete-search-pro', [CompraController::class, 'selectSearch']);

Route::get('/maestro-proveedor/update/{id}', [MaestroProveedorController::class, 'update_form'])->name('proveedor.form-update');

Route::post('maestro-proveedor/editar', [MaestroProveedorController::class, 'update'])->name('proveedor.editar');

Route::get('/maestro-proveedor/detalles/{id}', [MaestroProveedorController::class, 'proveedor'])->name('proveedor.datos');

/* Usuarios Routes*/
Route::get('/usuarios', [UsuariosController::class, 'index'])->name('usuarios.index');
Route::put('/usuarios/update', [UsuariosController::class, 'update'])->name('usuario.update');
Route::put('/usuarios/update/password', [UsuariosController::class, 'update'])->name('usuario.updatePassword');

/* Venta y cuentas de venta Routes*/
Route::get('/venta', [VentaController::class, 'index'])->name('venta.index');
Route::post('/venta/create', [VentaController::class, 'store'])->name('venta.store');
Route::post('/venta/update', [VentaController::class, 'update'])->name('venta.update');
Route::delete('/venta/destroy', [VentaController::class, 'destroy'])->name('venta.delete');
Route::put('/venta/pay', [VentaController::class, 'pay'])->name('venta.pay');
Route::put('/venta/edit/pay', [VentaController::class, 'editPay'])->name('venta.edit.pay');
//-- Detalles de venta --//
Route::get('/venta/detalle-venta/{id}', [DetalleVentaController::class, 'index'])->name('detalleVenta.index');

Route::get('/venta/{id}', [VentaController::class, 'venta'])->name('venta.edit-form');

Route::post('/venta/detalle-venta/create', [DetalleVentaController::class, 'store'])->name('detalleVenta.create');
Route::put('/compra/detalle-venta/update', [DetalleVentaController::class, 'update'])->name('detalleVenta.update');
Route::delete('/compra/detalle-venta/destroy', [DetalleVentaController::class, 'destroy'])->name('detalleVenta.delete');


/* compra y cuentas de compra Routes*/
Route::get('/compra/{id}', [CompraController::class, 'index'])->name('compra.index');
Route::put('/compra/update', [CompraController::class, 'update'])->name('compra.update');
Route::delete('/compra/destroy', [CompraController::class, 'destroy'])->name('compra.delete');
//-- Detalles de compra --//
Route::get('/compra/detalle-compra/{id}', [DetalleCompraController::class, 'index'])->name('detalleCompra.index');
Route::post('/compra/detalle-compra/create', [DetalleCompraController::class, 'store'])->name('detalleCompra.create');
Route::put('/compra/detalle-compra/update', [DetalleCompraController::class, 'update'])->name('detalleCompra.update');
Route::delete('/compra/detalle-compra/destroy', [DetalleCompraController::class, 'destroy'])->name('detalleCompra.delete');

//-- Cuentas Por Pagar --//
Route::put('/compra/pagar/iva', [CuentasPagarController::class, 'iva'])->name('compra.pagar.iva');
Route::put('/compra/pagar/retencion', [CuentasPagarController::class, 'retencion'])->name('compra.pagar.retencion');
Route::put('/compra/cobrar/comision', [CuentasCobrarController::class, 'comision'])->name('compra.cobrar.comision');






/*/-- PAIS --/*/
Route::get('pais/paraiso-fiscal', [PaisController::class, 'index'])->name('pais.index');
Route::put('pais/paraiso-fiscal', [PaisController::class, 'update'])->name('pais.update');

/*/-- Estado --/*/
Route::get('estado/paraiso-fiscal', [EstadoController::class, 'index'])->name('estado.index');
Route::put('estado/paraiso-fiscal', [EstadoController::class, 'update'])->name('estado.update');

/*--   Cuentas X Cobrar  --*/
Route::get('/cuentas-x-pagar', [CuentasPagarController::class, 'index'])->name('cuentas-pagar.index');


Route::get('/', [VentaController::class, 'index'])->name('administrador.index');


Auth::routes(['register' => false]);